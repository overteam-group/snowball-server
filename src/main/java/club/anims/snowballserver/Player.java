package club.anims.snowballserver;

import club.anims.snowballserver.entities.PlayerEntity;
import lombok.*;

import java.net.Socket;

@Getter @Setter @Builder @AllArgsConstructor @NoArgsConstructor
public class Player {
    private PlayerEntity playerEntity;
    private Socket socket;
}
