package club.anims.snowballserver;

import lombok.*;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;

@Builder @Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class SocketData {
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
}
