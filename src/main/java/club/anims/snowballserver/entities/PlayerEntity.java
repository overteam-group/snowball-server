package club.anims.snowballserver.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class PlayerEntity extends SquareEntity {
    private String name;

    public PlayerEntity(String name, float x, float y, float width, float height, String color) {
        super(x, y, width, height, color);
        this.name = name;
    }
}
