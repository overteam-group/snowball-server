package club.anims.snowballserver.requests;

import lombok.*;

@Getter @Setter @Builder @NoArgsConstructor @AllArgsConstructor
public class UpdateRequest {
    private float x;
    private float y;
}
