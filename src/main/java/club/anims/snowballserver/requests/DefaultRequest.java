package club.anims.snowballserver.requests;

import lombok.*;

@Getter @Setter @Builder @NoArgsConstructor @AllArgsConstructor
public class DefaultRequest {
    private String command;
    private String data;
}
