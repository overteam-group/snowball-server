package club.anims.snowballserver;

import club.anims.snowballserver.entities.PlayerEntity;
import club.anims.snowballserver.requests.BeginRequest;
import club.anims.snowballserver.requests.DefaultRequest;
import club.anims.snowballserver.requests.UpdateRequest;
import com.google.gson.Gson;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

@Getter @Setter
public class SnowballServerThread extends Thread {
    private SnowballServer serverInstance;
    private SocketData socketData;
    private final Logger LOGGER = LoggerFactory.getLogger(SnowballServerThread.class);

    public SnowballServerThread(Socket socket, SnowballServer serverInstance) {
        this.socketData = SocketData.builder().socket(socket).build();
        this.serverInstance = serverInstance;
    }

    @Override
    public void run() {
        try {
            socketData.setOut(new PrintWriter(socketData.getSocket().getOutputStream(), true));
            socketData.setIn(new BufferedReader(new InputStreamReader(socketData.getSocket().getInputStream())));
            String input;
            while ((input = socketData.getIn().readLine()) != null) {
                var output = "";
                var gson = new Gson();
                DefaultRequest request;
                try{
                    request = gson.fromJson(input, DefaultRequest.class);
                }catch (Exception e){
                    output = "invalid request";
                    socketData.getOut().println(output);
                    continue;
                }
                switch (request.getCommand()) {
                    case "begin" -> {
                        BeginRequest beginRequest;
                        try{
                            beginRequest = gson.fromJson(request.getData(), BeginRequest.class);
                        }catch (Exception e) {
                            output = "invalid request";
                            break;
                        }
                        var player = Player.builder()
                                .socket(socketData.getSocket())
                                .playerEntity(new PlayerEntity(beginRequest.getName(), beginRequest.getX(), beginRequest.getY(), beginRequest.getWidth(), beginRequest.getHeight(), beginRequest.getColor()))
                                .build();
                        this.serverInstance.getPlayers().add(player);
                        output = "connection established: player successfully connected";
                        LOGGER.info("player connected: {} from {}", player.getPlayerEntity().getName(), player.getSocket().getInetAddress());
                    }

                    case "fetch" -> {
                        output = gson.toJson(this.serverInstance.getPlayers().stream().filter(p -> !p.getSocket().equals(this.socketData.getSocket())).map(Player::getPlayerEntity).toArray());
//                        LOGGER.debug("{}", output);
                        LOGGER.debug("player {} fetched", this.serverInstance.getPlayers().stream().filter(player -> player.getSocket().equals(this.socketData.getSocket())).findFirst().get().getPlayerEntity().getName());
                    }

                    case "update" -> {
                        UpdateRequest updateRequest;
                        try{
                            updateRequest = gson.fromJson(request.getData(), UpdateRequest.class);
                        }catch (Exception e) {
                            output = "invalid request";
                            break;
                        }
                        var player = this.serverInstance.getPlayers().stream().filter(p -> p.getSocket().equals(this.socketData.getSocket())).findFirst().orElse(null);
                        if (player == null) {
                            output = "error: player not found";
                            break;
                        }
                        this.serverInstance.getPlayers().stream().filter(p -> p.getSocket().equals(this.socketData.getSocket())).forEach(p -> {
                            p.getPlayerEntity().setX(updateRequest.getX());
                            p.getPlayerEntity().setY(updateRequest.getY());
                        });
                        output = "player updated";
                        LOGGER.debug("player {} updated", player.getPlayerEntity().getName());
                    }

                    default -> {
                        output = "default response: request accepted";
                        LOGGER.debug("default request accepted to {}", this.socketData.getSocket().getInetAddress());
                    }
                }
                socketData.getOut().println(output);
            }
            socketData.getOut().close();
            socketData.getIn().close();
            socketData.getSocket().close();
        }catch (SocketException e){
            LOGGER.info("player disconnected: {}", this.socketData.getSocket().getInetAddress());
            SnowballServer.instance.getPlayers().removeIf(p -> p.getSocket().equals(this.socketData.getSocket()));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
