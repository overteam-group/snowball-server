package club.anims.snowballserver;

import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ServerSocket;
import java.util.ArrayList;

@Getter
public class SnowballServer {
    public static SnowballServer instance;

    private static final int DEFAULT_PORT = 6969;

    private final int PORT;
    private ServerSocket serverSocket;
    private ArrayList<Player> players;
    private final Logger LOGGER = LoggerFactory.getLogger(SnowballServer.class);

    public SnowballServer(int port){
        this.PORT = port;
        players = new ArrayList<>();
    }

    public void start(){
        try{
            serverSocket = new ServerSocket(PORT);
            LOGGER.info("Server started on port {}", PORT);
            while (true)
                new SnowballServerThread(serverSocket.accept(), this).start();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        var port = 0;
        try{
            port = new Port(args[0]).get();
        }catch (Exception e){
            if(args.length > 0){
                System.out.println(e.getMessage());
                System.exit(0);
            }
            else{
                port = DEFAULT_PORT;
//                System.out.println("Usage: java -jar snowballserver.jar <port>");
//                System.exit(0);
            }
        }
        instance = new SnowballServer(port);
        instance.start();
    }
}